#! /usr/bin/env python
#coding:utf-8
"""二维码测试，根据指定url生成二维码的文件
包安装 sudo pip install qrcode
"""
import qrcode

url="http://www.baidu.com"
q=qrcode.main.QRCode()
q.add_data(url)
q.make()
m=q.make_image()
m.save('/home/guolt/test.png')

