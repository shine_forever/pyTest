#! /usr/bin/python
#coding:utf-8
"""
多线程实现ping多主机
"""
__author__ = 'guolt'

import subprocess
from Queue import Queue
from threading import Thread

queue=Queue()
ips=['172.16.41.25','172.16.41.26','172.16.41.254']

def pinger(i,q):
    while True:
        ip=q.get()
        print "Thread %s : pinging: %s \n" %(i,ip)
        ret=subprocess.call('ping -c 1 %s ' % ip,shell=True,stdout=open('/dev/null','w'),stderr=subprocess.STDOUT)
        if ret == 0:
            print "%s : is alive " % ip
        else:
            print "%s : did not respond" % ip
        q.task_done()

for i in range(3):
    th = Thread(target=pinger,args=(i,queue))
    th.setDaemon(True)
    th.start()


for ip in ips:
    queue.put(ip)

print "Main Thread waiting......"
queue.join()
print "Job Done!!!!"