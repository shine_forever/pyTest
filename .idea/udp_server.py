#/usr/bin/python
#coding:utf-8
__author__ = 'guolt'

from socket import *
from time import ctime

#定义变量
HOST=''
PORT=21567
BUFSIZ=1024
ADDR = (HOST,PORT)

udpSerSock = socket(AF_INET,SOCK_DGRAM)
udpSerSock.bind(ADDR)

while True:
    print 'waiting for message.....'
    data,addr = udpSerSock.recvfrom(BUFSIZ)
    udpSerSock.sendto('[%s] %s' % (ctime(),data),addr)
    print '............received from and return to: ',addr

udpSerSock.close()