#! /usr/bin/env python
#coding:utf-8
#socket测试，多client，与socketSer.py配合；

from socket import *

HOST='127.0.0.1'
PORT=18000
ADDR=(HOST,PORT)
BUFSIZ=2048

data="........test message from client!"
if __name__ =="__main__":
    tcpCliSock=socket(AF_INET,SOCK_STREAM)
    tcpCliSock.connect(ADDR)
    tcpCliSock.sendall('%s\r\n' % data)
    data=tcpCliSock.recvfrom(BUFSIZ)
    tcpCliSock.close()
