#! /usr/bin/python
#coding:utf-8
#monitor server

import SocketServer
import datetime
BUFSIZ=2048

class myMonitorHandler(SocketServer.BaseRequestHandler):
    "This is the Monitor server!!!"
    def handle(self):
        recv_data=self.request.recv(BUFSIZ)
        print "From %s: %s %s" %(self.client_address,datetime.datetime.now(),recv_data)

if __name__ == "__main__":
    host,port='',18000
    server=SocketServer.ThreadingTCPServer((host,port),myMonitorHandler)
    server.serve_forever()