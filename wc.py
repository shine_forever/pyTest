#! /usr/bin/env python
#coding:utf-8

"""
使用python打开随机文件，并且查看文件一共多少行！
"""

from sys import argv as getargv
filename=getargv[1]
def CountLines(filename):
    countlines=''
    countlines=len(open(filename).readlines())
    return countlines

def GetFileInfo(filename):
    countlines=CountLines(filename)
    print '文件名称:%s,行数:%s' %(filename,countlines)

if __name__ == '__main__':
#    print filename
#    a=CountLines(filename)
#    print a
    GetFileInfo(filename)
