#! /usr/bin/python
#coding:utf-8
#tcp类型的socket测试；

import socket
from time import ctime,sleep



HOST='0.0.0.0'
PORT=50007
ADDR=(HOST,PORT)
BUFSIZ=1024

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind(ADDR)
s.listen(5)



while 1:
    print 'waiting for connection.....'
    conn,addr=s.accept()
    print '....Connected from',addr

    while True:
        data = conn.recv(BUFSIZ)
        sleep(1)
        if not data:break
        conn.sendall('[%s] %s' %(ctime(),data))
        conn.close()
s.close()