#! /usr/bin/python
#coding:utf-8
#利用socket模块，判断远程服务器port是否能访问;


import socket

def checkPort(server,port):
    s=socket.socket()
    try:
        s.connect((server,port))
        print "success...!connected"
        return True
    except socket.error,e:
        print "connection to host %s on port %s failure!" % (server,port)
        return False


#应用checkPort发方法：
checkPort('www.sharev.org',22)
