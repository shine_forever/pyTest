#! /usr/bin/env python
#coding:utf-8
def test(*arg,**kvarg):
#tuple output
    print "arg is :",arg
    print "+"*10
#dic output
    print "kvarg is:",kvarg
    print "##"*10

test(1,2,3)
test(1,3,5,a=1,b=30,c=20)


