#! /usr/bin/python
#coding:utf-8
#SocketServer服务器测试，可以同时接受多个多个socket


from SocketServer import (TCPServer as TCP,StreamRequestHandler as SRH)
from time import ctime
import os

HOST=''
PORT=21567
ADDR=(HOST,PORT)
BUFSIZ=4096

class Myserver(SRH):
    def handle(self):
        print '......connected from: ',self.client_address
        self.data=self.request.recv(BUFSIZ).strip()
        print 'received message is: %s' % self.data
        cmd=os.popen(self.data)
        result=cmd.read()
        self.request.sendall(result)



tcpServ=TCP(ADDR,Myserver)
print 'waiting for connecting.....'
tcpServ.serve_forever()