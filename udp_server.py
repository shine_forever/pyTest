# /usr/bin/env python
#coding:utf-8
#udp服务端测试;
__author__ = 'guolt'

from socket import *
from time import ctime

#申明变量;
HOST=''
PORT=21567
BUFSIZ=1024
ADDR=(HOST,PORT)

#udp的服务器端，不需要listen
udpSerSock=socket(AF_INET,SOCK_DGRAM)
udpSerSock.bind(ADDR)

while True:
    print 'waiting for message.......'
    data,addr=udpSerSock.recvfrom(BUFSIZ)
    udpSerSock.sendto('[%s] %s' %(ctime(),data),addr)
    print '......received messages from cilent ip address: ',addr

udpSerSock.close()
