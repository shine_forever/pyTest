#! /usr/bin/python
#coding:utf-8
#利用smtplib模块,通过routon邮箱认证后，发送邮件！

import smtplib
from email.mime.text import MIMEText


def send_mail(sub,content):
#群发邮件列表
    mailto_list=["305959613@qq.com","shine_forever@yeah.net"]
#发送邮件的主机名
    mail_host="mail.routon.com"
#邮件认证相关
    mail_user='guoletao@routon.com'
    mail_pass='xxxxxx'
    mail_postfix="routon.com"

###############################
    """
    to_list:发给谁
    sub:邮件的主题
    content:邮件内容
    send_mail("test@163.com","sub","content")
    """
################################


    me=mail_user+"("+mail_user+"@"+mail_postfix+")"
    msg = MIMEText(content,_charset='gbk')
    msg['Subject'] = sub
    msg['From'] = me
    msg['To'] = ";"
    try:
        s=smtplib.SMTP()
        s.connect(mail_host)
        s.login(mail_user,mail_pass)
        s.sendmail(me,mailto_list,msg.as_string())
        s.close()
        return True
    except Exception,e:
        print str(e)
        return False

if __name__ == '__main__':
    if send_mail(u'<python测试邮件标题>',u'这是邮件的正文，谢谢！'):
        print u'邮件发送成功，请登录邮箱查收！'
    else:
        print u'发送失败，请检查无误后再发！'


