#! /usr/bin/env python
#coding:utf-8
import logging
import time
from logging.handlers import SMTPHandler


# 创建一个logger
logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)

# 创建一个handler，用于写入日志文件
fh = logging.FileHandler('/home/guolt/test.log')
fh.setLevel(logging.INFO)

# 再创建一个handler，用于输出到控制台
ch = SMTPHandler(mailhost='mail.routon.com',
                                fromaddr='erpadmin@routon.com',
                                toaddrs=['guoletao@routon.com'],
                                subject='check_cdn Warning!',
                                credentials=('erpadmin','erpadmin'),
                                secure=None)
ch.setLevel(logging.WARNING)

# 定义handler的输出格式
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)

# 给logger添加handler
logger.addHandler(fh)
logger.addHandler(ch)

# 记录一条日志
logger.info('This is info message!')
logger.warning('测试邮件。。')

